# frozen_string_literal: true

module EE
  module Integrations
    module IssueTracker
      def create_cross_reference_note
        # implement inside child
      end
    end
  end
end
